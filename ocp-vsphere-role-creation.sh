#!/bin/bash

case $1 in
    "create-roles")
        govc role.create openshift-vcenter-level \
        Cns.Searchable \
        InventoryService.Tagging.AttachTag \
        InventoryService.Tagging.CreateCategory \
        InventoryService.Tagging.CreateTag \
        InventoryService.Tagging.DeleteCategory \
        InventoryService.Tagging.DeleteTag \
        InventoryService.Tagging.EditCategory \
        InventoryService.Tagging.EditTag \
        Sessions.ValidateSession \
        StorageProfile.Update \
        StorageProfile.View

        govc role.create openshift-resourcepool-level \
        Host.Config.Storage \
        Resource.AssignVMToPool \
        VApp.AssignResourcePool \
        VApp.Import \
        VirtualMachine.Config.AddNewDisk

        govc role.create openshift-datastore-level \
        Datastore.AllocateSpace \
        Datastore.Browse \
        Datastore.FileManagement \
        InventoryService.Tagging.ObjectAttachable

        govc role.create openshift-portgroup-level \
        Network.Assign

        govc role.create openshift-datacenter-level \
        InventoryService.Tagging.ObjectAttachable \
        Resource.AssignVMToPool \
        VApp.Import \
        VirtualMachine.Config.AddExistingDisk \
        VirtualMachine.Config.AddNewDisk \
        VirtualMachine.Config.AddRemoveDevice \
        VirtualMachine.Config.AdvancedConfig \
        VirtualMachine.Config.Annotation \
        VirtualMachine.Config.CPUCount \
        VirtualMachine.Config.DiskExtend \
        VirtualMachine.Config.DiskLease \
        VirtualMachine.Config.EditDevice \
        VirtualMachine.Config.Memory \
        VirtualMachine.Config.RemoveDisk \
        VirtualMachine.Config.Rename \
        VirtualMachine.Config.ResetGuestInfo \
        VirtualMachine.Config.Resource \
        VirtualMachine.Config.Settings \
        VirtualMachine.Config.UpgradeVirtualHardware \
        VirtualMachine.Interact.GuestControl \
        VirtualMachine.Interact.PowerOff \
        VirtualMachine.Interact.PowerOn \
        VirtualMachine.Interact.Reset \
        VirtualMachine.Inventory.Create \
        VirtualMachine.Inventory.CreateFromExisting \
        VirtualMachine.Inventory.Delete \
        VirtualMachine.Provisioning.Clone \
        VirtualMachine.Provisioning.DeployTemplate \
        VirtualMachine.Provisioning.MarkAsTemplate \
        Folder.Create \
        Folder.Delete

        govc role.create openshift-cluster-level \
        Host.Config.Storage \
        Resource.AssignVMToPool \
        VApp.AssignResourcePool \
        VApp.Import \
        VirtualMachine.Config.AddNewDisk
    ;;

    "apply-roles")
        govc permissions.set -principal=openshift@vsphere.local -role=openshift-vcenter-level -propagate=false /
        govc permissions.set -principal=openshift@vsphere.local -role=openshift-datacenter-level -propagate=true /Home
        govc permissions.set -principal=openshift@vsphere.local -role=openshift-cluster-level -propagate=true /Home/host/LAB
        govc permissions.set -principal=openshift@vsphere.local -role=openshift-datastore-level -propagate=false /Home/datastore/vmdata-2
        govc permissions.set -principal=openshift@vsphere.local -role=ReadOnly -propagate=false /Home/network/DSW-Default
        govc permissions.set -principal=openshift@vsphere.local -role=openshift-portgroup-level -propagate=false /Home/network/OCP-2
    ;;

    "cleanup")
        govc permissions.remove -principal=openshift@vsphere.local /
        govc permissions.remove -principal=openshift@vsphere.local /Home
        govc permissions.remove -principal=openshift@vsphere.local /Home/host/LAB
        govc permissions.remove -principal=openshift@vsphere.local /Home/datastore/vmdata-2
        govc permissions.remove -principal=openshift@vsphere.local /Home/network/DSW-Default
        govc permissions.remove -principal=openshift@vsphere.local /Home/network/OCP-2
        govc role.remove openshift-vcenter-level
        govc role.remove openshift-resourcepool-level
        govc role.remove openshift-datastore-level
        govc role.remove openshift-portgroup-level
        govc role.remove openshift-folder-level
        govc role.remove openshift-datacenter-level
        govc role.remove openshift-cluster-level
    ;;
esac
